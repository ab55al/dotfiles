#!/bin/python3

from sys import argv
from os import getcwd, getenv

programs = [
    "zig build",
    "zig test",
    "zig run",
    "go run",
    "go build",
    "ocaml",
    'odin run',
    'odin build',
]


def get_path() -> str:
    home = getenv("HOME")
    if home is None or len(home) == 0:
        print("-- Home value is invalid --")
        exit(1)

    return home + "/.local/share/compile_commands_list"


def store_cmd(args: str):
    with open(get_path(), 'a+') as file:
        file.seek(0)
        content = file.read()

        line = getcwd() + "," + args + "\n"
        new_content = line + content.replace(line, "")  # de-duplicate

        file.seek(0)
        file.truncate()
        file.write(new_content)


def delete_cmd(args: str):
    with open(get_path(), 'a+') as file:
        file.seek(0)
        content = file.read()
        line = getcwd() + "," + args + "\n"
        new_content = content.replace(line, "")

        if new_content == content:
            print("Nothing changed")

        file.seek(0)
        file.truncate()
        file.write(new_content)


if (len(argv) == 1):
    exit(1)
elif argv[1] == "--list":
    with open(get_path(), 'a+') as file:
        file.seek(0)
        print(file.read())

elif argv[1] == "--list-in-cwd":
    result_list = []
    with open(get_path(), 'a+') as file:
        file.seek(0)
        content = file.read()
        cwd = getcwd() + ','
        for line in content.splitlines(keepends=True):
            comma_index = line.find(',')
            if cwd.startswith(line[:comma_index + 1]):
                result_list.append(line[comma_index + 1:])

    result = ""
    # dict from list keeps things ordered unlike a set
    for cmd in dict.fromkeys(result_list):
        result += cmd

    # Remove last newline
    print(result[0:len(result) - 1])

elif argv[1] == "--add" and len(argv) > 2:
    args = ""
    for arg in argv[2:]:
        args += arg + " "
    args = args[0:len(args) - 1]

    store_cmd(args)

elif argv[1] == "--add-from-list" and len(argv) > 2:
    args = ""
    for arg in argv[2:]:
        args += arg + " "
    args = args[0:len(args) - 1]

    for p in programs:
        if (args.startswith(p)):
            store_cmd(args)

elif argv[1] == "--delete" and len(argv) > 2:
    args = ""
    for arg in argv[2:]:
        args += arg + " "
    args = args[0:len(args) - 1]
    delete_cmd(args)
else:
    print("Unknown option")
    exit(1)
